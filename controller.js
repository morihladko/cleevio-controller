var cleevio = cleevio || {};

(function(global, cleevio) {
	'use strict';
	var BulletClass = Bullet.constructor;

	function Button(key, keyCode) {
		var self = this;

		BulletClass.call(this);

		this._pressed = true;

		this.key = key;
		this.keyCode = keyCode;

		this.on('keydown', function() {
			self._pressed = true;
		});

		this.on('keyup', function() {
			self._pressed = false;
		});
	};

	Button.prototype = Object.create(BulletClass.constructor.prototype);
	Button.constructor = Button;

	Button.prototype.isDown = function() {
		return this._pressed
	};

	function Controller() {
		var self = this;

		BulletClass.call(this);

		this.buttons = {};

		this.on('keydown', function(data) {
			if (!data.button || !self.buttons[data.button]) {
				return;
			}

			self.buttons[data.button].trigger('keydown');
		});

		this.on('keyup', function(data) {
			if (!data.button || !self.buttons[data.button]) {
				return;
			}

			self.buttons[data.button].trigger('keyup');
		});

		// export for native calls
		cleevio.controller = this;
	};

	Controller.prototype = Object.create(BulletClass.constructor.prototype);
	Controller.constructor = Controller;

	Controller.prototype.addButton = function(button) {
		this.buttons[button.key] = button;
	};

	Controller.prototype.enableKeyboard = function() {
		var key, button, keyCodes = {}, self = this;

		for (key in this.buttons) {
			button = this.buttons[key];

			if (button.keyCode) {
				keyCodes[button.keyCode] = button;
			}
		}

		// TODO odstranit zavislost na jQuery/Zepto
		$(window).on('keydown', function(ev) {
			var button = keyCodes[ev.keyCode];

			if (!button) {
				return;
			}

			ev.preventDefault();
			self.trigger('keydown', {button: button.key});

		}).on('keyup', function(ev) {
			var button = keyCodes[ev.keyCode];

			if (!button) {
				return;
			}

			ev.preventDefault();
			self.trigger('keyup', {button: button.key});
		});
	};

	function LeftRightController() {
		Controller.call(this);

		this.addButton(new Button('left', 37));
		this.addButton(new Button('right', 39))
	};

	LeftRightController.prototype = Object.create(Controller.prototype);
	LeftRightController.prototype.constructor = LeftRightController;

	// export
	cleevio.Controller = Controller;
	cleevio.LeftRightController = LeftRightController;
}(this, cleevio));
